FROM gitlab-registry.oit.duke.edu/devops/containers/debian-buster:latest

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install certbot; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

VOLUME /etc/letsencrypt /var/www/html

ENTRYPOINT [ "certbot" ]

CMD [ "--help" ]
