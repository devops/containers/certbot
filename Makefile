SHELL = /bin/bash

build_tag ?= certbot

.PHONY: build
build:
	docker build -t $(build_tag) - < ./Dockerfile
